﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using FlashCodeSolution.IO;
using FlashCodeSolution.Models;
using FlashCodeSolution.Models.AtOwnRisk;

namespace FlashCodeSolution
{
    class Program
    {
        public static string BaseUri = "http://judgesystem.azurewebsites.net";
        private static string ApiKey = "API KEY HERE";

        static async Task Main(string[] args)
        {
            var totalTimer = new Stopwatch();
            var watch = new Stopwatch();
            var totalScore = 0;
            watch.Start();
            watch.Stop();

            var client = new FlashPost(ApiKey);

            var assignmentDetails = await client.GetAssignmentIds();

            totalTimer.Start();
            foreach (var assignmentDetail in assignmentDetails)
            {
                var assignment = await client.GetAssignmentById(assignmentDetail.Id);
                watch.Restart();
                var output = ProcessInput(assignment);
                watch.Stop();
                var score = await client.SubmitAssignment(assignmentDetail.Id, output);
                Console.WriteLine($"{assignmentDetail.Name}: {watch.ElapsedMilliseconds} ms Score: {score}");
                totalScore += score;
            }

            totalTimer.Stop();
            Console.WriteLine($"Total Time: {totalTimer.Elapsed.TotalSeconds}s");
            Console.WriteLine($"Total Score: {totalScore}");
        }

        private static IEnumerable<VehicleAssignment> ProcessInput(InputModel input)
        {
            var output = new List<VehicleAssignment>(input.Vehicles); // fill me up pls
            for (int i = 0; i < input.Vehicles; i++)
            {
                output[i] = new VehicleAssignment();
            }
            /*
             * Todo: Put Code here
             */
            return output;
        }
    }
}