﻿using System;
using FlashCodeSolution.Models.AtOwnRisk;

namespace FlashCodeSolution
{
    public static class Utils
    {
        public static int CalcDistance(Coordinate start, Coordinate end)
        {
            var distance = 0;

            distance += Math.Abs(start.Column - end.Column);
            distance += Math.Abs(start.Row - end.Row);
            return distance;
        }
    }
}