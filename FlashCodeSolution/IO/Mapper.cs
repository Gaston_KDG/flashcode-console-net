using System.Collections.Generic;
using System.Linq;
using FlashCodeSolution.Models.AtOwnRisk;

namespace FlashCodeSolution.IO
{
    public static class Mapper
    {
        public static InputModel MapInput(string[] input)
        {
            var header = input[0].Split(' ');
            var inputModel = new InputModel()
            {
                Rows = int.Parse(header[0]),
                Columns = int.Parse(header[1]),
                Vehicles = int.Parse(header[2]),
                RideCount = int.Parse(header[3]),
                BonusPoints = int.Parse(header[4]),
                Steps = int.Parse(header[5])
            };
            inputModel.RideDefinitions = new RideDefinition[inputModel.RideCount];

            for (int i = 1; i < 1 + inputModel.RideCount; i++)
            {
                var split = input[i].Split(' ');
                var startCoordinate = new Coordinate(int.Parse(split[0]), int.Parse(split[1]));
                var endCoordinate = new Coordinate(int.Parse(split[2]), int.Parse(split[3]));
                
                inputModel.RideDefinitions[i - 1] = new RideDefinition()
                {
                    RideId = i-1,
                    StartCoordinate = startCoordinate,
                    EndCoordinate = endCoordinate,
                    EarliestStart = int.Parse(split[4]),
                    LatestFinish = int.Parse(split[5]),
                    Distance = Utils.CalcDistance(startCoordinate,endCoordinate)
                };
            }

            return inputModel;
        }

        public static string MapOutput(IEnumerable<VehicleAssignment> outputModel)
        {
            var strings = outputModel
                .Select(va => $"{va.RideIds.Count()} {string.Join(' ', va.RideIds)}");
            return string.Join("\n", strings);
        }
    }
}