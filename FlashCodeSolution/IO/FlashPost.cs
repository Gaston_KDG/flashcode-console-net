using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FlashCodeSolution.Models.AtOwnRisk;
using Refit;

namespace FlashCodeSolution.IO
{
    public class FlashPost
    {
        private readonly string _apiKey;
        private readonly IJudgeApi _client;

        public FlashPost(string apiKey)
        {
            _apiKey = apiKey;
            _client = RestService.For<IJudgeApi>(Program.BaseUri);
        }

        public async Task<AssignmentDetails[]> GetAssignmentIds()
        {
            var response = await _client.GetIds(_apiKey);
            return response;
        }

        public async Task<InputModel> GetAssignmentById(Guid id)
        {
            var response = await _client.GetProblemFile(id, _apiKey);
            var content = Encoding.UTF8.GetString(response).Trim();
            return Mapper.MapInput(content.Split("\n"));
        }

        public async Task<int> SubmitAssignment(Guid id, IEnumerable<VehicleAssignment> output)
        {
            var outputString = Mapper.MapOutput(output);
            var score = await _client.SubmitProblem(id, Encoding.UTF8.GetBytes(outputString), _apiKey);
            return score;
        }
    }
}