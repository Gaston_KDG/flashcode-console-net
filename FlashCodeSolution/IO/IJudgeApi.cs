using System;
using System.Threading.Tasks;
using FlashCodeSolution.Models.AtOwnRisk;
using Refit;

namespace FlashCodeSolution.IO
{
    interface IJudgeApi
    {
        [Get("/api/problem")]
        Task<AssignmentDetails[]> GetIds([Header("apiKey")] string apiKey);
        
        [Get("/api/problem/{problemId}")]
        Task<byte[]> GetProblemFile(Guid problemId, [Header("apiKey")] string apiKey);
        
        [Post("/api/submit/{problemId}")]
        Task<int> SubmitProblem(Guid problemId, [Body] byte[] content, [Header("apiKey")] string apiKey);
    }
}