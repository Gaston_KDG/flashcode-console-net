using System;
using System.Collections.Generic;
using FlashCodeSolution.Models.AtOwnRisk;

namespace FlashCodeSolution.Models
{
    internal class Vehicle
    {
        public Vehicle()
        {
            Rides = new List<RideDefinition>();
            StepsTaken = 0;
            Position = new Coordinate(0, 0);
        }

        public int StepsTaken { get; set; }
        public List<RideDefinition> Rides { get; }
        public Coordinate Position { get; set; }
    }
}