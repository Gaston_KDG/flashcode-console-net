using System.Collections.Generic;

namespace FlashCodeSolution.Models.AtOwnRisk
{
    public class VehicleAssignment
    {
        public VehicleAssignment()
        {
            RideIds = new List<int>();
        }

        public IEnumerable<int> RideIds { get; set; }
    }
}