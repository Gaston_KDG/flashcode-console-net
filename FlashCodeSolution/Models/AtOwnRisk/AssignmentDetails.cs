using System;

namespace FlashCodeSolution.Models.AtOwnRisk
{
    public class AssignmentDetails
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}