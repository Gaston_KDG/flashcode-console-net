namespace FlashCodeSolution.Models.AtOwnRisk
{
    public class InputModel
    {
        public int Rows { get; set; }
        public int Columns { get; set; }
        public int Vehicles { get; set; }
        public int RideCount { get; set; }
        public int BonusPoints { get; set; }
        public int Steps { get; set; }
        public RideDefinition[] RideDefinitions { get; set; }
    }
}