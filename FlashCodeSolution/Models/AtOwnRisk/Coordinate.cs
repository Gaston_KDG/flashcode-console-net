using System;

namespace FlashCodeSolution.Models.AtOwnRisk
{
    public struct Coordinate
    {
        public Coordinate(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public int Row { get; }
        public int Column { get; }

        public int DistanceTo(Coordinate other)
        {
            return Utils.CalcDistance(this, other);
        }

        public bool Equals(Coordinate other)
        {
            return Row == other.Row && Column == other.Column;
        }

        public override bool Equals(object obj)
        {
            return obj is Coordinate other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Row, Column);
        }
    }
}