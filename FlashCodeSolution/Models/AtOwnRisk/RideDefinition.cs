namespace FlashCodeSolution.Models.AtOwnRisk
{
    public class RideDefinition
    {
        public int RideId { get; set; }
        public Coordinate StartCoordinate { get; set; }
        public Coordinate EndCoordinate { get; set; }
        public int EarliestStart { get; set; }
        public int LatestFinish { get; set; }
        public int Distance { get; set; }
    }
}